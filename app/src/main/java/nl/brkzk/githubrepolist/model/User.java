package nl.brkzk.githubrepolist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Boris on 25/02/2017.
 */

public class User {


    public String login;
    public String name;

    @SerializedName("public_repos")
    public int publicRepoCount;

}
