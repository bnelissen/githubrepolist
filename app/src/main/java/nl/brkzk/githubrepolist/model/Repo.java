package nl.brkzk.githubrepolist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Boris on 25/02/2017.
 */

public class Repo {

    public String name;

    @SerializedName("created_at")
    public String createDate;

}
