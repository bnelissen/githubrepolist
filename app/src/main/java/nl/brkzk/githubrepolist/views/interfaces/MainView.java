package nl.brkzk.githubrepolist.views.interfaces;

import java.util.List;

import nl.brkzk.githubrepolist.model.Repo;
import nl.brkzk.githubrepolist.model.User;

/**
 * Created by Boris on 25/02/2017.
 */
public interface MainView {

    void showLoading();
    void hideLoading();

    void showDialog(long deltaTime);
    void showError();

    void disableButton();
    void enableButton();

    void setUserText(String login, String name, int repoCount);
    void setRepos(List<Repo> repos);

}
