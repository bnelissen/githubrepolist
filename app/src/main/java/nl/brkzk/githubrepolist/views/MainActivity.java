package nl.brkzk.githubrepolist.views;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import nl.brkzk.githubrepolist.presenter.MainPresenter;
import nl.brkzk.githubrepolist.R;
import nl.brkzk.githubrepolist.model.Repo;
import nl.brkzk.githubrepolist.views.interfaces.MainView;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements MainView {

    DisposableSingleObserver subscription;

    @ViewById(R.id.progress_bar)
    ProgressBar progressBar;

    @ViewById(R.id.list_view)
    ListView listView;

    @ViewById(R.id.user_text_view)
    TextView userTextView;

    @ViewById(R.id.button)
    Button button;

    @Bean
    MainPresenter presenter;

    @AfterViews
    void afterViews() {
        presenter.setView(this);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialog(long deltaTime) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_connection_delta_title)
                .setMessage(String.format(getString(R.string.dialog_connection_delta_message), deltaTime))
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.connection_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void disableButton() {
        button.setEnabled(false);
    }

    @Override
    public void enableButton() {
        button.setEnabled(true);
    }

    @Click(R.id.button)
    public void buttonClicked() {
        presenter.buttonClicked();
    }

    @Override
    public void setUserText(String login, String user, int repoCount) {
        userTextView.setText(String.format("Login: %s \nName: %s \nRepos: %d", login, user, repoCount));
    }

    @Override
    public void setRepos(List<Repo> repos) {
         subscription =
                 Observable.fromIterable(repos)
                         .subscribeOn(Schedulers.io())
                         .map(new Function<Repo, String>() {
                             @Override
                             public String apply(Repo repo) throws Exception {
                                 return String.format("%s\ncreated: %s", repo.name, repo.createDate);
                            }
                        })
                         .observeOn(AndroidSchedulers.mainThread())
                         .toList()
                         .subscribeWith(getObserver());
    }

    @Override
    protected void onDestroy() {
        presenter.disposeView();
        if(subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }
        super.onDestroy();
    }

    public DisposableSingleObserver<List<String>> getObserver() {
        return new DisposableSingleObserver<List<String>>() {
            @Override
            public void onSuccess(List<String> value) {
                listView.setAdapter(new ArrayAdapter<String>(MainActivity.this, R.layout.list_item, R.id.text_view, value));
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }
}
