package nl.brkzk.githubrepolist.presenter;

import android.util.Log;

import org.androidannotations.annotations.EBean;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import nl.brkzk.githubrepolist.model.Repo;
import nl.brkzk.githubrepolist.model.User;
import nl.brkzk.githubrepolist.retrofit.GithubApiInterface;
import nl.brkzk.githubrepolist.views.interfaces.MainView;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@EBean(scope = EBean.Scope.Singleton)
public class MainPresenter {

    public static String USERNAME = "JakeWharton";

    private GithubApiInterface githubService;
    private MainView mainView;

    private DisposableObserver mainObserver;

    private User user;
    private List<Repo> repos;

    public MainPresenter() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        githubService = retrofit.create(GithubApiInterface.class);
    }

    public void setView(MainView view) {
        if(mainObserver != null) {
            mainObserver.dispose();
            mainObserver = null;
        }
        mainView = view;
        updateMainView();
    }

    public void disposeView() {
        if(mainObserver != null) {
            mainObserver.dispose();
        }
        mainView = null;
    }

    private void updateMainView() {
        if(user != null && repos != null) {
            mainView.setUserText(user.login, user.name, user.publicRepoCount);

            // Instead of passing repos directly to the view
            // mapping them to a List<String> here first is
            // probably more in line with the MVP pattern
            //
            // Feedback appreciated

            mainView.setRepos(repos);
        }

        mainView.hideLoading();
        mainView.enableButton();
    }

    public void buttonClicked() {
        mainView.disableButton();

        mainObserver = Observable.merge(githubService.getUser(USERNAME), githubService.getRepoList(USERNAME))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver());

        mainView.showLoading();
    }

    public DisposableObserver<Object> getObserver() {
        return new DisposableObserver<Object>() {

            long timer = 0;
            long deltaTime;

            User newUser;
            List<Repo> newRepos;

            @Override
            public void onNext(Object value) {
                if(value instanceof User) {
                    newUser = (User) value;
                }
                else if(value instanceof List) {
                    newRepos = (List<Repo>) value;
                }

                if(timer == 0) {
                    timer = System.currentTimeMillis();
                }
                else {
                    deltaTime = System.currentTimeMillis() - timer;
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(MainPresenter.this.toString(), "Error on Requests");
                updateMainView();
                mainView.showError();
            }

            @Override
            public void onComplete() {
                user = newUser;
                repos = newRepos;

                updateMainView();
                mainView.showDialog(deltaTime);
            }
        };
    }

}
