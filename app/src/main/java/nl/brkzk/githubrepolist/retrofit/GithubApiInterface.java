package nl.brkzk.githubrepolist.retrofit;

import java.util.List;

import io.reactivex.Observable;
import nl.brkzk.githubrepolist.model.Repo;
import nl.brkzk.githubrepolist.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Boris on 25/02/2017.
 */

public interface GithubApiInterface {

    @GET("/users/{username}")
    Observable<User>
    getUser(@Path("username") String username);

    @GET("/users/{username}/repos")
    Observable<List<Repo>>
    getRepoList(@Path("username") String username);

}
